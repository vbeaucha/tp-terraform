number_of_jenkins_slave = 3

ingress_ports = [80, 443]

ingress_rules_configuration = [
  {
    port : 80,
    protocol : "tcp",
    cidr_blocks : ["192.168.0.0/16","10.0.0.0/16"]
  },
  {
    port : 443,
    protocol : "tcp",
    cidr_blocks : ["0.0.0.0/0"]
  }
]