<!--- 
Ceci est la version en markdown !
Utilisez l'aperçu pour avoir une version plus lisible
-->
# Terraform - TP5: Passage à l'échelle

> **Objectifs du TP** :
>- Modulariser le code
>
> **Prérequis :**
>- Une machine avec un navigateur
>- Un accès à internet
>
> **Niveau de difficulté** : Intermédiaire

## 1- Objectifs détaillés

Dans le TP 4, nous avions vu comment...

Nous allons voir maintenant comment factoriser notre code, en utilisant les modules:

## 2- Utilisation du module "my_ec2_instance"

Nous avons déjà préparé un module appelé my_ec2_instance disponibles dans le dossier modules qui déploit:
* une instance
* un security group avec un certain nombre de règle (https, ssh)

Editer le fichier topology.tf pour y appeler le module my_ec2_instance pour créer une instance webserver

Ensuite faire un deuxième appel à ce module pour y créer une instance database

Vérifier/corriger la syntaxe terraform:
```bash
$ terraform fmt
```

Initialiser le provider:
```bash
$ terraform init
```

Visualiser le plan d'exécution:
```bash
$ terraform plan
```

Appliquer le plan d'exécution:
```bash
$ terraform apply
```


## 3- Mise à jour du module

Maintenant que l'on est capable de créer plusieurs type d'instance. 
On aimerait bien pouvoir y ajouter des disks lorsqu'il y en a besoin. 
Par exemple pour une database

Dans le module my_ec2_instance, éditez les fichiers suivants pour:

Ajoutez dans le fichier **variables.tf**, les variables suivantes:
* disks: de type list(objet) avec comme valeur par défaut une liste vide
    * chaque object contiendra 4 champs: (type, size, name_suffix, device_path) 

Ajoutez dans le fichier **topology.tf** les resource suivantes :
* aws_ebs_volume (pour la partie availability_zone, utilisez la même que celle du subnet)
* aws_volume_attachment

La variable disks étant une liste, il faudra ajouter autant de disk que d'objet dans la liste


Vérifier/corriger la syntaxe terraform:
```bash
$ terraform fmt
```

Visualiser le plan d'exécution:
```bash
$ terraform plan
```

Appliquer le plan d'exécution:
```bash
$ terraform apply
```

Pour valider l'ajout des disques rajoutons un disque à l'instance database en le définissant lors de l'appel du module

## 4- Bonus

Vous pouvez utiliser le mode console pour récupérer l’ip de l’instance et le volume id de l’instance:

Exemple:
```bash
$ terraform console
> module.webserver.instance.private_ip
172.32.2.14
> module.database.instance.private_ip
172.32.2.14
```

## 5- Un petit coup de ménage

Pour détruire votre topology :
```bash
$ terraform destroy
```
