
resource "aws_subnet" "my_subnet" {
  availability_zone = "us-east-1c"
  vpc_id            = "vpc-0b4048156c0de9005"
  cidr_block        = "10.0.4.0/24"


  tags = {
    Name        = "subnet-TF-TP2-VABE"
    "Formation" = "terraform"
    "User"      = "VABE"
  }
}

resource "aws_security_group" "my_security_group" {
  vpc_id      = "vpc-0b4048156c0de9005"
  name_prefix = "TF-TP2_SG"

  tags = {
    Name      = "sg-TF-TP2-VABE"
    Formation = "terraform"
    "User"    = "VABE"
  }
}

resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_in_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_out_https" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks = ["::/0"]
  security_group_id = aws_security_group.my_security_group.id
}



resource "aws_instance" "my_instance" {
  ami           = "ami-0d73480446600f555" # Ubuntu AMI
  instance_type = "t3.medium"             # Taille de l'instance https://www.ec2instances.info/
  subnet_id     = aws_subnet.my_subnet.id
  vpc_security_group_ids = [aws_security_group.my_security_group.id]


  tags = {
    Name      = "instance-TF-TP2-VABE"
    Formation = "terraform"
    User      = "VABE"
  }

}